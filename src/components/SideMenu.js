import * as React from 'react';

import {
  CalendarMonth,
  ConfirmationNumber,
  Dashboard,
  DonutLarge,
  FactCheck,
  FeaturedPlayList,
  Group,
  GroupAdd,
  PanoramaVerticalSelect,
  Settings,
  Share,
} from '@mui/icons-material';

import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function SideMenu(open = true) {
  const { t } = useTranslation();

  const navigate = useNavigate();
  return (
    <>
      <Divider />
      <List>
        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            onClick={() => {
              navigate('/dashboard');
            }}
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <DonutLarge />
            </ListItemIcon>
            <ListItemText
              primary='Dashboard'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <FeaturedPlayList />
            </ListItemIcon>
            <ListItemText
              primary='Item 2'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <CalendarMonth />
            </ListItemIcon>
            <ListItemText
              primary='Item 3'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <GroupAdd />
            </ListItemIcon>
            <ListItemText
              primary='Item 4'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <Group />
            </ListItemIcon>
            <ListItemText
              primary='Item 5'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <Dashboard />
            </ListItemIcon>
            <ListItemText
              primary='Item 6'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <PanoramaVerticalSelect />
            </ListItemIcon>
            <ListItemText
              primary='Item 7'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <FactCheck />
            </ListItemIcon>
            <ListItemText
              primary='Item 8'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <ConfirmationNumber />
            </ListItemIcon>
            <ListItemText
              primary='Item 9'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <Share />
            </ListItemIcon>
            <ListItemText
              primary='Item 10'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>

        <ListItem disablePadding sx={{ display: 'block' }}>
          <ListItemButton
            sx={{
              minHeight: 48,
              justifyContent: open ? 'initial' : 'center',
              px: 2.5,
            }}
          >
            <ListItemIcon
              sx={{
                minWidth: 0,
                /* mr: open ? 3 : 'auto', */
                justifyContent: 'center',
              }}
            >
              <Settings />
            </ListItemIcon>
            <ListItemText
              primary='Item 11'
              sx={{
                opacity: open ? 1 : 0,
                ml: document.body.dir === 'ltr' ? 3 : 0,
                mr: document.body.dir === 'rtl' ? 3 : 0,
              }}
            />
          </ListItemButton>
        </ListItem>
      </List>
    </>
  );
}
