import 'react-calendar/dist/Calendar.css';

import * as React from 'react';

import {
  Balcony,
  BorderVertical,
  Business,
  MovieCreationOutlined,
  Sledding,
  TimerOutlined,
  TrendingDown,
  TrendingUp,
} from '@mui/icons-material';

import Calendar from 'react-calendar';
import { Chart } from 'react-google-charts';
import { goldenColor } from '../utilities/globelVariabels';
import { useTranslation } from 'react-i18next';

export default function Detail() {
  const { t } = useTranslation();

  return (
    <div className='row px-1'>
      <div className='col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12'>
        <div className='px-1'>
          <div className='row rounded-card'>
            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='p-1'>
                <div className='d-flex justify-content-between align-items-center border-bottom mb-2'>
                  <div className='h6'>{t('h1')}</div>
                  <div className='h4 text-golden'>50000</div>
                </div>
                <Chart
                  chartType='Bar'
                  loader={<snap>{t('t1')}</snap>}
                  style={{
                    color: 'red',
                    backgroundColor: 'black',
                    borderColor: 'yellow',
                  }}
                  data={[
                    ['', ''],
                    ['Jan', 6000],
                    ['Feb', 8000],
                    ['Mar', 3000],
                    ['Apr', 5000],
                    ['May', 2000],
                    ['Jun', 7000],
                    ['Jul', 6000],
                    ['Aug', 4000],
                    ['Sep', 10000],
                    ['Oct', 9000],
                  ]}
                  width='100%'
                  height='200px'
                />
              </div>
            </div>
          </div>

          <div className='row rounded-card my-2'>
            <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12'>
              <div className='p-1'>
                <div className='border-bottom mb-2'>
                  <div className='h6'>{t('h2')}</div>
                </div>
                <div style={{ maxHeight: '300px', overflowY: 'scroll' }}>
                  <table class='table table-borderless'>
                    <tbody>
                      <tr>
                        <td>{t('t2')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t4')}</td>
                      </tr>
                      <tr>
                        <td>{t('t1')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t6')}</td>
                      </tr>
                      <tr>
                        <td>{t('t7')}</td>
                        <td>{t('t4')}</td>
                        <td>{t('t2')}</td>
                      </tr>
                      <tr>
                        <td>{t('t2')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t4')}</td>
                      </tr>
                      <tr>
                        <td>{t('t1')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t6')}</td>
                      </tr>
                      <tr>
                        <td>{t('t7')}</td>
                        <td>{t('t4')}</td>
                        <td>{t('t2')}</td>
                      </tr>
                      <tr>
                        <td>{t('t2')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t4')}</td>
                      </tr>
                      <tr>
                        <td>{t('t1')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t6')}</td>
                      </tr>
                      <tr>
                        <td>{t('t7')}</td>
                        <td>{t('t4')}</td>
                        <td>{t('t2')}</td>
                      </tr>
                      <tr>
                        <td>{t('t2')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t4')}</td>
                      </tr>
                      <tr>
                        <td>{t('t1')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t6')}</td>
                      </tr>
                      <tr>
                        <td>{t('t7')}</td>
                        <td>{t('t4')}</td>
                        <td>{t('t2')}</td>
                      </tr>
                      <tr>
                        <td>{t('t2')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t4')}</td>
                      </tr>
                      <tr>
                        <td>{t('t1')}</td>
                        <td>{t('t3')}</td>
                        <td>{t('t6')}</td>
                      </tr>
                      <tr>
                        <td>{t('t7')}</td>
                        <td>{t('t4')}</td>
                        <td>{t('t2')}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='col-xl-7 col-lg-7 col-md-12 col-sm-12 col-xs-12'>
        <div className='px-1'>
          <div className='row rounded-card'>
            <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='row' style={{ height: '100%' }}>
                <div className='col-lg-6 col-md-6 col-sm-12 p-1'>
                  <div
                    className='card rounded-card gray-gradient p-2 d-flex justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='d-flex align-items-center'>
                      <Sledding />
                      <span className='h6'>{t('h4')}</span>
                    </div>
                    <div className='h1'>+20K</div>
                    <div className='d-flex justify-content-between'>
                      <span>{t('t2')}</span>
                      <span>
                        60.3% <TrendingUp />
                      </span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-12 p-1'>
                  <div
                    className='card rounded-card pink-gradient p-2 d-flex justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='d-flex align-items-center'>
                      <Balcony />
                      <span className='h6'>{t('h6')}</span>
                    </div>
                    <div className='h1'>+20K</div>
                    <div className='d-flex justify-content-between'>
                      <span>{t('t2')}</span>
                      <span>
                        40.3% <TrendingDown />
                      </span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-12 p-1'>
                  <div
                    className='card rounded-card gray-gradient p-2 d-flex justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='d-flex align-items-center'>
                      <BorderVertical />
                      <span className='h6'>{t('h3')}</span>
                    </div>
                    <div className='h1'>+20K</div>
                    <div className='d-flex justify-content-between'>
                      <span>{t('h5')}</span>
                      <span>
                        54.3% <TrendingUp />
                      </span>
                    </div>
                  </div>
                </div>

                <div className='col-lg-6 col-md-6 col-sm-12 p-1'>
                  <div
                    className='card rounded-card pink-gradient p-2 d-flex justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='d-flex align-items-center'>
                      <Business fontSize='small' />
                      <div className='h5'>{t('h6')}</div>
                    </div>
                    <div className='h1'>+20K</div>
                    <div className='d-flex justify-content-between'>
                      <span>{t('t1')}</span>
                      <span>
                        20.3 <TrendingDown />
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='row p-1'>
                <div className='card rounded-card'>
                  <Calendar
                    className='text-center border-0'
                    onChange={() => {}}
                    value={new Date()}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className='row rounded-card my-2'>
            <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12'>
              <div className='row p-1'>
                <div className='card rounded-card p-2'>
                  <div className='d-flex justify-content-between align-items-center border-bottom mb-2'>
                    <div className='h6'>{t('h9')}</div>
                    <div className='h3 text-golden'>2000</div>
                  </div>
                  <div>
                    <Chart
                      chartType='Bar'
                      loader={<snap>Loading</snap>}
                      style={{
                        color: 'red',
                        backgroundColor: 'black',
                        borderColor: 'yellow',
                      }}
                      data={[
                        ['', ''],
                        ['Jan', 6000],
                        ['Feb', 8000],
                        ['Mar', 3000],
                        ['Apr', 5000],
                        ['May', 2000],
                        ['Jun', 7000],
                        ['Jul', 6000],
                        ['Aug', 4000],
                        ['Sep', 10000],
                        ['Oct', 9000],
                      ]}
                      width='100%'
                      height='200px'
                    />
                  </div>
                </div>
              </div>
            </div>

            <div className='col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6'>
              <div className='row' style={{ height: '100%' }}>
                <div className='col-lg-12 col-md-12 col-sm-12 p-1'>
                  <div
                    className='card rounded-card d-flex flex-col justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='mt-4'>
                      <span className='p-2 bg-golden rounded'>
                        <MovieCreationOutlined />
                      </span>
                    </div>
                    <div className='mb-4'>
                      <div>{t('h3')}</div>
                      <div className='h3'>{t('t1')}</div>
                      <div className='mt-3 text-golden'>
                        28.3 <TrendingUp />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className='col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-6'>
              <div className='row' style={{ height: '100%' }}>
                <div className='col-lg-12 col-md-12 col-sm-12 p-1'>
                  <div
                    className='card rounded-card d-flex flex-col justify-content-between'
                    style={{ height: '100%' }}
                  >
                    <div className='mt-4'>
                      <span className='p-2 bg-golden rounded'>
                        <TimerOutlined />
                      </span>
                    </div>
                    <div className='mb-4'>
                      <div>{t('h4')}</div>
                      <div className='h3'>{t('h5')}</div>
                      <div className='mt-3 text-golden'>
                        20.3 <TrendingUp />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
