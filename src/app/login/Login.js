import {
  IconButton,
  InputAdornment,
  OutlinedInput,
  TextField,
} from '@mui/material';
import React, { useState } from 'react';
import { Visibility, VisibilityOff } from '@mui/icons-material';

import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default function Login() {
  const [showPassword, setShowPassword] = useState(false);
  const navigate = useNavigate();
  const { t } = useTranslation();
  const onSubmit = () => {
    localStorage.setItem('user', true);
    navigate('/dashboard');
    document.location.reload();
  };

  return (
    <div
      style={{
        height: '100vh',
        width: '100%',
        backgroundImage: 'url("assets/images/background.png")',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}
    >
      <div className='container'>
        <div className='row align-items-center'>
          <div className='col-sm-3 col-md-6 col-lg-7'></div>
          <div
            className='col-sm-9 col-md-6 col-lg-5 d-flex align-items-center'
            style={{ height: '100vh', width: '100%' }}
          >
            <div
              className='card border-0 shadow my-3'
              style={{ borderRadius: '20px' }}
            >
              <div className='card-body p-4 p-sm-5'>
                <div className='d-flex'>
                  <img src='assets/images/logo.png' alt='logo' />
                  <div
                    className='mx-2'
                    style={{ border: '1px solid', backgroundColor: 'black' }}
                  ></div>
                  <h4>Shawara Portal</h4>
                </div>
                <h5 className='card-title my-4 fw-light fs-5'>Sign In</h5>
                <form onSubmit={onSubmit}>
                  <div className='mb-2'>
                    <label className='mb-1' htmlFor='floatingInput'>
                      Email address
                    </label>
                    <TextField
                      type='email'
                      className='border-0'
                      style={{ border: '0', backgroundColor: '#ffe6ff' }}
                      id='floatingInput'
                      placeholder='name@example.com'
                      size='small'
                      fullWidth
                    />
                  </div>
                  <div className='mb-3'>
                    <label className='mb-1' htmlFor='floatingPassword'>
                      Password
                    </label>
                    <OutlinedInput
                      id='floatingPassword'
                      style={{ border: '0', backgroundColor: '#ffe6ff' }}
                      fullWidth
                      size='small'
                      type={showPassword ? 'text' : 'password'}
                      endAdornment={
                        <InputAdornment position='end'>
                          <IconButton
                            aria-label='toggle password visibility'
                            onClick={() => setShowPassword(!showPassword)}
                            edge='end'
                          >
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </div>

                  <div className='d-flex flex-wrap justify-content-between mb-3'>
                    <div style={{ fontSize: f13 }}>
                      <input
                        className='form-check-input'
                        type='checkbox'
                        value=''
                        id='rememberPasswordCheck'
                      />
                      <label
                        className='form-check-label'
                        for='rememberPasswordCheck'
                      >
                        Remember password
                      </label>
                    </div>
                    <div style={{ fontSize: f13 }}>
                      Did you forget the password?
                    </div>
                  </div>
                  <div className='text-center'>
                    <button
                      className='btn btn-dark btn-login text-uppercase px-5 fw-bold'
                      type='submit'
                    >
                      Sign in
                    </button>
                  </div>
                  <div style={{ fontSize: f13 }} className='text-center'>
                    <span>You do not have an account?</span>
                    <span>Create an account</span>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const f13 = '13px';
