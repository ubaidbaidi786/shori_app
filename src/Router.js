import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useEffect, useState } from 'react';

import Home from './app/Home';
import Login from './app/login/Login';
import MasterPage from './components/MasterPage';

function Router() {
  const [user, setUser] = useState(true);
  useEffect(() => {
    setUser(localStorage.getItem('user'));
  }, [localStorage.getItem('user')]);
  return (
    <BrowserRouter>
      <Routes>
        {!user ? (
          <>
            <Route path='*' element={<Login />}></Route>
          </>
        ) : (
          <Route
          path='*'
            element={
              <MasterPage>
                <Routes>
                  <Route path='/' element={<Home />}></Route>
                  <Route path='/dashboard' element={<Home />}></Route>
                </Routes>
              </MasterPage>
            }
          />
        )}
      </Routes>
    </BrowserRouter>
  );
}

export default Router;
